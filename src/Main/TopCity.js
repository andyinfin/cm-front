import React, { Component } from 'react';

class TopCity extends Component {
  render() {
    return (
        <div className="Top-item Top-city">
            <p>{this.props.name}</p>
        </div>
    );
  }
}

export default TopCity;