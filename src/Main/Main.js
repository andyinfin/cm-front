import React, { Component } from 'react';
import { connect } from 'react-redux';
import TopMayor from './TopMayor';
import TopCity from './TopCity';
import Button from './../components/Button';
import ReactTooltip from 'react-tooltip';
import { Link } from 'react-router-dom';
import axios from 'axios';

class Main extends Component {
    constructor(props) {
        super(props);

        this.getTopCities = this.getTopCities.bind(this);
        this.getTopMayors = this.getTopMayors.bind(this);
        this.buildNewCity = this.buildNewCity.bind(this);
    
        this.state = {
            // В топах будут заглушки, если бэк не присылает данные (например, выдаёт false или null)
            // Если бэк отдаёт пустой массив - заглушки уже не будет!!!
            topCities: [
                { id: 1, name: 'Moscow' },
                { id: 2, name: 'Saint-Petersburg' },
                { id: 3, name: 'Novosibirsk' }
            ],
            topMayors: [
                { id: 1, name: 'Mayor #1' },
                { id: 2, name: 'Mayor #2' },
                { id: 3, name: 'Mayor #3' }
            ]
        };
    }

    async getTopCities() {
        try {
            let response = await axios.get(this.props.baseUrl + '/api/top_cities');
            if (response.data) {
                this.setState({ topCities: response.data });
            }
        } catch (e) {
            console.log(e);
        }
    }

    async getTopMayors() {
        try {
            let response = await axios.get(this.props.baseUrl + '/api/top_mayors');
            if (response.data) {
                this.setState({ topMayors: response.data });
            }
        } catch (e) {
            console.log(e);
        }
    }

    async buildNewCity() {
        let account = window.web3.eth.defaultAccount;
        console.log('Building new city...');
        try {
            let response = await window.CryptoCitiesContract.addCity(account, '', { from: account });
            console.log('addCity response:', response);
            this.getTopCities(); // Refresh top cities after building
        } catch(e) {
            console.log(e);
        }
    }

    componentDidMount() {
        this.getTopCities();
        this.getTopMayors();
    }

    render() {
        return (
            <div className="App-container Main-container Flex-center">
                <ReactTooltip place="bottom" />
                <h1>Top Cities</h1>
                <Button tooltip="Start building!" text="Build new city" onClick={() => this.buildNewCity()} />
                <div className="Top-container">
                    {
                        this.state.topCities.map( (city, index) => <Link to={"/city/" + city.id} key={index}><TopCity name={city.name} /></Link> )
                    }
                </div>
                
                <h1>Top Mayors</h1>
                <div className="Top-container">
                    {
                        this.state.topMayors.map( (mayor, index) => <Link to={"/mayor/" + mayor.id} key={index}><TopMayor name={mayor.name} /></Link> )
                    }
                </div>
            </div>
        );
    }
}

export default connect(
    state => ({
        baseUrl: state.baseUrl,
        contracts: state.contracts
    }),
    dispatch => ({})
)(Main);