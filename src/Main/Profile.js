import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import axios from 'axios';

class Profile extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
            citiesArray: [],
            mayorCities: [],
            citizenCities: []
        };
    }

    async getCities() {
        try {
            let response = await axios.get(this.props.baseUrl + '/api/cities');
            if (response.data) {
                this.setState({ citiesArray: response.data });
                this.getCitizenCities();
                this.getMayorCities();
            }
        } catch(e) {
            console.log(e);
        }
    }

    async getCitizenCities() {
        let account = window.web3.eth.defaultAccount;
        let instance = window.CryptoCitizensContract;
        let cities = this.state.citiesArray;
        try {
            let response = await instance.getCitizenCities(account);    
            if (response.length > 0) {
                response.forEach(e => {
                    let id = e.toNumber();
                    let city = cities.find(f => f.id === id.toString());
                    this.state.citizenCities.push(city.name + ' (id: ' + id + ')');
                });
            }
            this.forceUpdate();
        } catch(e) {
            console.log(e);
        }
    }

    async getMayorCities() {
        let account = window.web3.eth.defaultAccount;
        let instance = window.CryptoCitiesContract;
        let cities = this.state.citiesArray;
        try {
            let response = await instance.getMayorCities(account);    
            if (response.length > 0) {
                response.forEach(e => {
                    let id = e.toNumber();
                    let city = cities.find(f => f.id === id.toString());
                    this.state.mayorCities.push(city.name + ' (id: ' + id + ')');
                });
            }
            this.forceUpdate();
        } catch(e) {
            console.log(e);
        }
    }
    
    componentDidMount() {
        this.getCities();
    }

    render() {
        return (
            <div className="Main-container Flex-center">
                <ul>
                    <p>Mayor at:</p>
                    {this.state.mayorCities.map(function(elem, index) {
                        return (<li key={index}>{elem}</li>)
                    })}
                </ul>
                <ul>
                    <p>Citizen at:</p>
                    {this.state.citizenCities.map(function(elem, index) {
                        return (<li key={index}>{elem}</li>)
                    })}
                </ul>
                <Link to="/"><p className="Link-back">Back to main</p></Link>
            </div>
        );
    }
}

export default connect(
    state => ({
        baseUrl: state.baseUrl,
        contracts: state.contracts
    }),
    dispatch => ({})
)(Profile);