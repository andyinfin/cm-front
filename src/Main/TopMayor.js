import React, { Component } from 'react';

class TopMayor extends Component {
  render() {
    return (
        <div className="Top-item Top-mayor">
            <p>{this.props.name}</p>
        </div>
    );
  }
}

export default TopMayor;