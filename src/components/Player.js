import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Player.css';

class Player extends Component {
    componentDidMount() {}

    render() {
        return (
            <div className="Player-cont">
                <div className="Player-thumb">thumb</div>
                <Link to="/profile"><p>{this.props.name}</p></Link>
            </div>
        );
    }
}

export default Player;