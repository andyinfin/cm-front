import React, { Component } from 'react';
import './Button.css';

class Button extends Component {
    componentDidMount() {}

    render() {
        return (
            <button
                className="Common-button"
                data-tip={this.props.tooltip}
                onClick={this.props.onClick}
            >
                {this.props.text}
            </button>
        );
    }
}

export default Button;