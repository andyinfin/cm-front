import React, { Component } from 'react';
import { connect } from 'react-redux';
import Main from './Main/Main';
import City from './Cities/City';
import Mayor from './Mayors/Mayor';
import Profile from './Main/Profile';
import Player from './components/Player';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import './App.css';
// import { testFunc } from './Utils';
import Web3 from 'web3';
import axios from 'axios';
import contract from 'truffle-contract';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isContractsDeployed: false
        }
    }

    async deployContracts() {
        console.log('Deploying contracts');

        // CryptoCities deploy
        try {
            let response = await axios.get(this.props.baseUrl + '/CryptoCities.json');
            let MyContract = await contract(response.data);
            MyContract.setProvider(window.web3.currentProvider);
            let instance = await MyContract.deployed();
            window.CryptoCitiesContract = instance; // Make contract global
            console.log('CryptoCities deploy result:', instance);
            
            instance.contract_name = 'CryptoCities';
            // this.props.addContract(instance);

            let totalCities = (await instance.totalSupply()).toNumber();
            console.log('Cities total:', totalCities);
        } catch(e) {
            console.log(e);
        }

        // CryptoMayors deploy
        try {
            let response = await axios.get(this.props.baseUrl + '/CryptoMayors.json');
            let MyContract = await contract(response.data);
            MyContract.setProvider(window.web3.currentProvider);
            let instance = await MyContract.deployed();
            window.CryptoMayorsContract = instance; // Make contract global
            console.log('CryptoMayors deploy result:', instance);
        } catch(e) {
            console.log(e);
        }
        
        // CryptoCitizens deploy
        try {
            let response = await axios.get(this.props.baseUrl + '/CryptoCitizens.json');
            let MyContract = await contract(response.data);
            MyContract.setProvider(window.web3.currentProvider);
            let instance = await MyContract.deployed(); // Error
            window.CryptoCitizensContract = instance; // Make contract global
            console.log('CryptoCitizens deploy result:', instance);
        } catch(e) {
            console.log(e);
        }

        this.setState({ isContractsDeployed: true });
        console.log('Deploy complete!');
    }

    async initAccount() {
        let component = this; // Context of component
        let result = await window.web3.eth.getAccounts();    
        console.log('Active accounts:', result);
        if (!result.length) {
            alert('You must log into MetaMask account and then refresh page!');
        } else {
            let account = result[0];
            window.web3.eth.defaultAccount = account;
            // console.log('default account:', window.web3.eth.defaultAccount);
            component.deployContracts();
        }
    }

    componentDidMount() {
        // Is there is an injected web3 instance?
        if (typeof window.web3 !== 'undefined') {
            App.web3Provider = window.web3.currentProvider;
            window.web3 = new Web3(window.web3.currentProvider);
            console.log('Metamask check passed');
            this.initAccount();
        } else {
            // If no injected web3 instance is detected
            alert('Please install MetaMask extension and reload this page');
        }
        console.log('web3 init:', window.web3);
    }

    render() {
        return (
            <BrowserRouter>
                <div className="App">
                    <header className="App-header">
                        <h1 className="App-title">CryptoMayors.io</h1>
                        <div className="Header-ctrl">
                            <Player name="CryptoPlayer" />
                        </div>
                    </header>
                    <Switch
                        style={{  }}
                        isContractsDeployed={this.state.isContractsDeployed}
                    >
                        <Route exact path='/' component={Main} />
                        <Route path='/city/:number' component={City}/>
                        <Route path='/mayor/:number' component={Mayor}/>
                        <Route path='/profile' component={Profile}/>
                    </Switch>
                    <footer></footer>
                </div>
            </BrowserRouter>
        );
    }
}

// export default App;
export default connect(
    state => ({
        baseUrl: state.baseUrl,
        contracts: state.contracts
    }),
    dispatch => ({
        addContract: (contract) => {
            console.log('addContract:', contract);
            dispatch({ type: 'ADD_CONTRACT', payload: contract });
        }
    })
)(App);