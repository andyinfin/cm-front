import React, { Component } from 'react';

class Citizen extends Component {
  render() {
    return (
        <div className="Citizen">
            <p>{this.props.name}</p>
        </div>
    );
  }
}

export default Citizen;