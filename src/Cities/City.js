import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import axios from 'axios';
import './City.css';
import Citizen from './Citizen';

class City extends Component {
    constructor(props) {
        super(props);
    
        this.getSellPrice = this.getSellPrice.bind(this);
        this.changeMayor = this.changeMayor.bind(this);
        this.becomeCitizen = this.becomeCitizen.bind(this);

        this.state = {
            id: null,
            sellPriceWei: null,
            sellPriceEth: null,

            data: {
                id: null,
                desc: '',
                images: {
                    back: ''
                },
                name: '',
                owner: ''
            }
        };
    }
    
    async getSellPrice() {
        let instance = window.CryptoCitiesContract;
        let tokenId = this.props.match.params.number;
        if (instance !== undefined) {
            try {
                let priceWei = await instance.getSellPrice(tokenId);
                let priceEth = await window.web3.utils.fromWei(priceWei.toString());
                this.setState({ sellPriceWei: priceWei.toNumber(), sellPriceEth: priceEth });
            } catch(e) {
                console.log(e);
            }
        } else {
            setTimeout(this.getSellPrice, 250);
        }
    }

    async changeMayor() {
        let context = this;
        let account = window.web3.eth.defaultAccount;
        let instance = window.CryptoCitiesContract;
        let tokenId = this.props.match.params.number;
        let buyPrice = this.state.sellPriceWei;
        try {
            let response = await instance.changeMayor(account, tokenId, { from: account, value: buyPrice })
            console.log('changeMayor response:', response);
            context.getSellPrice(); // Refresh sell price
        } catch(e) {
            console.log(e);
        }
        
    }

    async becomeCitizen() {
        let account = window.web3.eth.defaultAccount;
        let instance = window.CryptoCitizensContract;
        let cityId = this.props.match.params.number;
        try {
            let citizenPrice = (await instance.getCitizenPrice(cityId)).toNumber();
            let response = await instance.joinCity(account, cityId, {from: account, value: citizenPrice});
            console.log('joinCity response:', response);
        } catch(e) {
            console.log(e);
        }
    }

    async componentDidMount() {
        this.setState({ id: this.props.match.params.number });
        try {
            this.getSellPrice();
            let response = await axios.get(this.props.baseUrl + '/api/city/' + this.props.match.params.number);
            if (response.data) {
                this.setState({ data: response.data });
            }
        } catch(e) {
            console.log(e);
        }
    }

    render() {
        return (
            <div className="Main-container Bottom-padding">
                <div className="Main-container No-padding">
                    <div className="City-title" style={{ backgroundImage: 'url(' + this.state.data.images.back + ')' }}>
                        <div className="Inner">
                            <h2>{this.state.data.name}</h2>
                            <span>
                                <p>Mayor: Unknown (owner)</p>
                                <button onClick={() => this.changeMayor()}>Become a mayor</button>
                            </span>
                            <p>Current price: {this.state.sellPriceEth} ETH</p>
                            <p>Rating: Unknown</p>
                            <p>Location: Unknown</p>
                            <span>
                                <p>Citizens: XX</p>
                                <button onClick={() => this.becomeCitizen()}>Become a citizen</button>
                            </span>
                        </div>
                    </div>
                    <p style={{ padding: '1em 2em', textAlign: 'left' }}>{this.state.data.desc}</p>
                    <div className="City-citizens">
                        <h2>Citizens</h2>
                        <div className="Top-container">
                            <Citizen name="Citizen 1" />
                            <Citizen name="Citizen 2" />
                            <Citizen name="Citizen 3" />
                        </div>
                    </div>
                </div>
                <Link to="/"><p className="Link-back">Back to main</p></Link>
            </div>
        );
    }
}

export default connect(
    state => ({
        baseUrl: state.baseUrl,
        contracts: state.contracts
    }),
    dispatch => ({})
)(City);