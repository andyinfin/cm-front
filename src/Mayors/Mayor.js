import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import axios from 'axios';
import './Mayor.css';

class Mayor extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
            id: null,
            data: {
                desc: 'Here comes description',
                name: 'Mayor'
            }
        };
      }
    
    async componentDidMount() {
        this.setState({ id: this.props.match.params.number });
        // Из стейта не получается сразу забрать id, выдаёт null, видимо связано с обработкой setState в реакте

        let response = await axios.get(this.props.baseUrl + '/api/mayor/' + this.props.match.params.number);
        try {
            // console.log('/api/mayor/id', response);
            if (response.data) {
                this.setState({ data: response.data });
            }
        } catch(e) {
            console.log(e);
        }
    }

    render() {
        return (
            <div className="Main-container Bottom-padding">
                <div className="Main-container No-padding">
                    <div className="Mayor-cont">
                        <div className="Inner">
                            <h2>Mayor: {this.state.data.name}</h2>
                        </div>
                    </div>
                    <p style={{ padding: '1em 2em', textAlign: 'left' }}>{this.state.data.desc}</p>
                </div>
                <Link to="/"><p className="Link-back">Back to main</p></Link>
            </div>
        );
    }
}

export default connect(
    state => ({
        baseUrl: state.baseUrl,
        contracts: state.contracts
    }),
    dispatch => ({})
)(Mayor);