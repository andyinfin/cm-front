/* Проверка, загружен ли MetaMask? */

// Is there is an injected web3 instance?
if (typeof web3 !== 'undefined') {
  App.web3Provider = web3.currentProvider;
  web3 = new Web3(web3.currentProvider);
} else {
  // If no injected web3 instance is detected, fallback to Ganache.
  App.web3Provider = new web3.providers.HttpProvider('http://127.0.0.1:7545');
  web3 = new Web3(App.web3Provider);
}


// Mining check
// web3.eth.isMining().then((result) => console.log(result))

// Get balance
// web3.eth.getBalance('0xEaEdcfC476b9e7C4909354E71264C3132e9D0253').then((result) => console.log(result/1000000000000000000))

// var MyContract = new web3.eth.Contract([]);

// console.log(window.web3.eth.getAccounts());

// let output = await instance.changeMayor(account, 1, {from: account});
// console.log('changeMayor result:', output);

// Window.abi = response.data.abi;
// await MyContract.defaults({ from: web3.eth.coinbase });