import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import './index.css';
import App from './App';
// import registerServiceWorker from './registerServiceWorker';

const initialState = {
    baseUrl: 'http://localhost:3000', // Back-end base URL
    contracts: []
};
  
function contracts(state = initialState, action) {
    if (action.type === 'ADD_CONTRACT') {
        return {
            ...state,
            contracts: [...state.contracts, action.payload]
        };
    }
    return state;
}

const store = createStore(contracts, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
// registerServiceWorker();